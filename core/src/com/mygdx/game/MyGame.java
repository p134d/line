package com.mygdx.game;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.StretchViewport;




public  class MyGame extends Game {
	 SpriteBatch batch;
    static MenuScreen mainscreen;
    static GameScreen gamescreen;

    @Override
	public void create () {
		batch = new SpriteBatch();
        mainscreen = new MenuScreen();
        mainscreen.setGame(this);
       setScreen(mainscreen);
      // setScreen(new InfinityScreen());

    }
    @Override
    public void render(){
        super.render();
    }

      public void showGame(){
          gamescreen = new GameScreen();
          this.setScreen(gamescreen);
      }


}