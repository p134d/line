package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Марина on 19.05.2017.
 */

public class InfinityScreen implements Screen {
Slider sliderball;
Table mainballs;
TextButton btnstart;
Level main;
Skin sliderskin;
Map stage;
TextButton.TextButtonStyle buttonstyle;
ArrayList<Integer> visibleballs;
ArrayList<MainBall> allballs;
Label select;
float H = Gdx.graphics.getHeight();
float W = Gdx.graphics.getWidth();










    public void InfinityScreen(){



    }






    @Override
    public void show() {
        main = Configuration.getLevel();
        sliderskin = new Skin();
        sliderskin.add("knob",new Texture("Interface/knob.png"));
        sliderskin.add("back",new Texture("Interface/slider.png"));
        Slider.SliderStyle sliderStyle= new Slider.SliderStyle(sliderskin.getDrawable("back"),sliderskin.getDrawable("knob"));
        sliderball = new Slider(2,main.usestypes.size(),1,false,sliderStyle);


        sliderball.setSize(W*0.75f,W*0.75f/8.73f);
        sliderball.getStyle().background.setMinWidth(sliderball.getWidth());
        sliderball.getStyle().background.setMinHeight(sliderball.getHeight());
        sliderball.setTouchable(Touchable.enabled);
        sliderball.setAnimateInterpolation(Interpolation.smoother);

        select = new Label("SELECT BALLS",new Label.LabelStyle(MenuScreen.mainfont, Color.DARK_GRAY));
        select.setFontScale(H/14/MenuScreen.fontsize);
        select.setWrap(true);

        select.setWidth(W*0.8f);



        mainballs = new Table();

        Skin start = new Skin(new TextureAtlas(Gdx.files.internal("Interface/data_start_btn.txt")));
        buttonstyle=new TextButton.TextButtonStyle(start.getDrawable("up"),start.getDrawable("down"),start.getDrawable("down"),MenuScreen.mainfont);

        stage  = new Map(new ScreenViewport());
        stage.addActor(select);
        Gdx.input.setInputProcessor(stage);

        btnstart= new TextButton("START",buttonstyle);
        btnstart.getLabel().setFontScale(H/9.2f/MenuScreen.fontsize);
        btnstart.setSize(W/1.5f,W/1.5f/3.5f);
        btnstart.setPosition(W/2-btnstart.getWidth()/2,H/10);


     sliderball.setPosition(W/2-sliderball.getWidth()/2,H*0.75f);
     sliderball.setValue(sliderball.getMaxValue());
     sliderball.getStyle().knob.setMinHeight(sliderball.getHeight()*0.8f);
     sliderball.getStyle().knob.setMinWidth(sliderball.getHeight()*0.8f);
     sliderball.setAnimateDuration(0.4f);

     select.setPosition(W/2-select.getWidth()/2,sliderball.getY()+Line.SIZE_LINE/2+sliderball.getStyle().background.getMinHeight());
     select.setAlignment(Align.center);



     allballs = new ArrayList<MainBall>();
     visibleballs=new ArrayList<Integer>();

     for (int i=0; i<main.usestypes.size();i++){
     MainBall m = new MainBall(main.usestypes.get(i));
     m.setSize(W/4,W/4);
     m.show=true;
     m.setTouchable(Touchable.disabled);
     m.setOrigin(m.getWidth()/2,m.getHeight()/2);
     allballs.add(m);
     visibleballs.add(main.usestypes.get(i));
     mainballs.add(m).size(W/4,W/4).pad(W/40).center();
     if ((i+1)%3==0){
         mainballs.row();
     }
     }
     mainballs.center();
     mainballs.setWidth(W);
     mainballs.setPosition(0,H*0.5f);
     mainballs.setTransform(false);




    stage.addActor(btnstart);
    stage.addActor(mainballs);
    stage.addActor(sliderball);
    addSliderListener();


    //Action

        select.addAction(Actions.moveTo(select.getX(),select.getY(),1f,Interpolation.smooth2));
        select.addAction(Actions.alpha(0,0));
        select.addAction(Actions.alpha(1,1f));
        select.setY(select.getY()*0.5f);

        sliderball.addAction(Actions.sequence(Actions.delay(0.6f),
                Actions.parallel(Actions.moveTo(sliderball.getX(),sliderball.getY(),0.5f,Interpolation.smooth),Actions.alpha(1,0.5f))));
        sliderball.setY(sliderball.getY()*0.4f);
        sliderball.addAction(Actions.alpha(0,0));

        mainballs.addAction(Actions.sequence(Actions.delay(0.9f),
                Actions.parallel(Actions.moveTo(mainballs.getX(),mainballs.getY(),0.5f,Interpolation.smooth),Actions.alpha(1,0.5f))));
        mainballs.setY(0);
        mainballs.addAction(Actions.alpha(0,0));

        btnstart.addAction(Actions.sequence(Actions.delay(1.1f),
                Actions.parallel(Actions.moveTo(btnstart.getX(),btnstart.getY(),0.5f,Interpolation.smooth),Actions.alpha(1,0.5f))));
        btnstart.setY(-btnstart.getHeight());
        btnstart.addAction(Actions.alpha(0,0));








    }


    private void addSliderListener(){
    sliderball.addListener(new ChangeListener() {
        @Override
        public void changed(ChangeEvent event, Actor actor) {
          Slider s=(Slider)actor;
          Gdx.app.log(visibleballs.size()+" ","");
          for (int i=2; i<s.getValue();i++){
              if (!visibleballs.contains(i)){
              MainBall m = allballs.get(i);
                  m.setPosition(mainballs.getCell(m).getActorX()+W/8,mainballs.getCell(m).getActorY()+W/8);
              m.setSize(0,0);
              allballs.get(i).setVisible(true);
              allballs.get(i).getActions().clear();
              allballs.get(i).addAction(Actions.parallel(Actions.sizeTo(W/4,W/4,0.5f,Interpolation.smooth),Actions.moveBy(m.getWidth()/2-W/8,m.getWidth()-W/8,0.5f,Interpolation.smooth)));
              visibleballs.add(i);
              Collections.sort(visibleballs);
              }
          }
          for (int i=(int)(s.getValue());i<visibleballs.size();i++){
          final  int k=i;
              MainBall m = allballs.get(i);
             m.setPosition(mainballs.getCell(m).getActorX(),mainballs.getCell(m).getActorY());
             m.setSize(W/4,W/4);
              visibleballs.remove(new Integer(i));
              allballs.get(i).getActions().clear();
              allballs.get(i).addAction(Actions.sequence(Actions.parallel(Actions.sizeTo(0,0,0.5f,Interpolation.smooth),
              Actions.moveBy(m.getWidth()/2,m.getWidth()/2,0.5f,Interpolation.smooth)),Actions.run(new Runnable() {
                  @Override
                  public void run() {
                      allballs.get(k).setVisible(false);
                  }
              })));
          }

        }
    });
        btnstart.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
            Configuration.typesinfinity=new ArrayList<Integer>();
            for (int i=0; i<visibleballs.size();i++){
                Configuration.typesinfinity.add(visibleballs.get(i));}
                MenuScreen.game.showGame();
                super.clicked(event, x, y);
            }
        });


    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(1,1,1,1);
        stage.act(delta);
        stage.draw();


    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
