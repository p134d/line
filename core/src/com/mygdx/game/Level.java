package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;

/**
 * Created by Марина on 05.04.2017.
 */

public class Level  {
    int NumLev;
    int attempts;
    static HashMap<Integer,Integer> types;
     HashMap<Integer,Integer> skills;
    ArrayList<Integer> usestypes= new ArrayList<Integer>();
    int generatecount;


    public LinkedList<Line> lines;
    int kmaxspeed;
    int kminspeed;
    int boost;
    int timenext;
    int maxball;
    int k;
    public static LinkedList strtype;

    public  void createStringTypes(){
       strtype = new LinkedList<String>();
       strtype.add("WATER BALL");
       strtype.add("FIRE BALL");
       strtype.add("METALL BALL");
       strtype.add("MAGNETIC BALL");
        strtype.add("WOODEN BALL");
        strtype.add("ACID BALL");
    }
    private void loadAllTypes(){
        types=new HashMap<Integer, Integer>();
        types.put(0,1);
        types.put(1,1);
        types.put(2,15);
        types.put(3,30);
        types.put(4,55);
        types.put(5,100);
    }




    public Level(JSONLevel main) {
        loadAllTypes();
        maxball = main.maxball;
        k = main.k;
        boost = main.boost;
        kmaxspeed = main.kmaxspeed;
        kminspeed = main.kminspeed;
        NumLev = main.NumLev;
        attempts = main.attempts;
        timenext = main.timenext;
        generatecount=main.generatecount;
        for (Integer a:main.types) {
        usestypes.add(a);
        }
        skills = new LinkedHashMap<Integer, Integer>();
        for (int i=0;i<3;i++){
            skills.put(i,main.skills.get(i));
        }
        Collections.sort(usestypes);
        lines = new LinkedList<Line>();
            if (main.lines.isEmpty()) {
                for (int i = 0; i < 3; i++) {
                    Line add = new Line(i + 1, i + 1, this);
                    add.Generate();
                    lines.add(add);
                }
                Gdx.app.log("УРОВЕНЬ ПУСТ","Level");
            } else {
                for (int i = 0; i < main.lines.size(); i++) {
                    try {
                        main.lines.get(i).setMainLevel(this);
                        lines.add(main.lines.get(i).clone());
                    } catch (CloneNotSupportedException e) {
                    }
                }
            }
            createStringTypes();
    }


    public Level(){
       skills = new HashMap<Integer, Integer>();
       skills.put(0,5);
      skills.put(1,5);
      skills.put(2,5);
        loadAllTypes();
        maxball=15;
        k=2300;
        boost=200;
        kmaxspeed=9700;
        kminspeed=1180;
        NumLev=1;
        timenext=2400;
        generatecount=5;
        usestypes.add(0);
        usestypes.add(1);
        usestypes.add(2);
        usestypes.add(3);
        usestypes.add(4);
        usestypes.add(5);
        lines=new LinkedList<Line>();
        for (int i=0; i<3;i++){
            Line add=new Line(i+1,i+1,this);
            add.Generate();
            lines.add(add);
        }
        createStringTypes();
    }

    public int[] AddNextSkill(){
    int[] a  = new int[skills.size()];
    int random;
        if (NumLev % 3==0){
        random = (int)(Math.random()*4);
            skills.put(2,skills.get(2)+random);
            a[2]=random;
        }
        random = (int)(Math.random()*2);
        skills.put(1,skills.get(1)+random);
        a[1]=random;
        random = (int)(Math.random()*2);
        skills.put(0,skills.get(0)+random);
        a[0]=random;
        return a;
    }

    public int[] AddSkill(){
    int[] a   =  new int[skills.size()];
    int add =(int)( Math.random()*100);
    if (add>95){
    int  random1 = (int)(Math.random()*skills.size());
    int random2 = (int)(Math.random()*4);
      a[random1]  =random2;
      skills.put(random1,random2);
    }
    return a;
    }

     public void GenerateLines(){
     if (generatecount>0){
         generatecount-=1;
         for (int i=0; i<lines.size();i++){
             lines.get(i).Generate();
         }
     }
     }



    public void nextLevel(){ // повышение сложности
         NumLev=NumLev+1;
         generatecount+=1;
        if (timenext>1000) {
            timenext -= 10;
        }
        if (NumLev % 3==0) {
            kmaxspeed+=30;
            kminspeed+=30;
        }
        for (Integer i: types.keySet()) {
            if (types.get(i)==NumLev && !usestypes.contains(types.get(i))) {
                addType(i);
                kmaxspeed-=(types.get(i)-types.get(i-1))/3*25;
                kminspeed-=(types.get(i)-types.get(i-1))/3*25;
                timenext=(int)(0.8f*timenext);
                maxball=(int)(maxball*0.7f);
                break;
            }
        }

        if (NumLev % 4==0){
        generatecount+=2;
            boost+=5;
            maxball+=3;
            if (k<2980){
                k+=20;
            }

        }
        for (int i=0; i<lines.size();i++){
            lines.get(i).Generate();
        }
        Configuration.SaveLevel(this);

    }

    public String loadDescriptionBall(int type){
        FileHandle file = Gdx.files.internal("Balls/"+type+".txt");
        String send = file.readString();
        return send;
    }

    public void addType(int type){
        usestypes.add(type);
        Collections.sort(usestypes);
    }

   static class JSONLevel{
   int generatecount;
        int NumLev;
        int attempts;
        LinkedList<Integer>types= new LinkedList<Integer>();
        public LinkedList<Line> lines=new LinkedList<Line>();
        LinkedList<Integer> skills = new LinkedList<Integer>();
        int kmaxspeed;
        int kminspeed;
        int boost;
        int timenext;
        int maxball;
        int k; // коэффициент сложности дорожек
       public JSONLevel(){
       }
        public JSONLevel(Level main){
        this.generatecount=main.generatecount;
            maxball=main.maxball;
            k=main.k;
            boost=main.boost;
            kmaxspeed=main.kmaxspeed;
            kminspeed=main.kminspeed;
            NumLev=main.NumLev;
            attempts=main.attempts;
            timenext=main.timenext;
            skills.clear();
            types.clear();
            lines.clear();
            for (int i=0; i<3;i++){
            Gdx.app.log("JSONSKILLS",""+main.skills.get(i));
               skills.add(main.skills.get(i));
            }
            for (Integer a:main.usestypes) {
            types.add(a);
            }
            if (!main.lines.isEmpty()){
                for (int i=0; i<main.lines.size();i++) {
                    try {
                        lines.add(main.lines.get(i).clone());
                    } catch (Exception e){
                    }
                }
            }

        }



    }

}
