package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;
import com.google.gson.Gson;

import java.util.ArrayList;

import javax.naming.ldap.PagedResultsControl;

/**
 * Created by Марина on 17.05.2017.
 */

public class Configuration {
    public static int MODE=1;
    public static ArrayList<Integer> typesinfinity;
    public static boolean pauseanimation=false;
    public static  int record;
    public static float k=1;
  //  public static int
    // 1- normal
    // 2 - infinity

    public static int getRecordInfinity(){
    Preferences preferences = Gdx.app.getPreferences("MAIN");
   record = preferences.getInteger("record");
     return record;
    }
    public static void setRecord(int r){
    Preferences preferences = Gdx.app.getPreferences("MAIN");
    preferences.putInteger("record",r);
    record=r;
    preferences.flush();
    }
    public Configuration() {
    }


    public static void SaveLevel(Level main){
    Preferences preferences = Gdx.app.getPreferences("MAIN");
        Json a = new Json();
        Level.JSONLevel jlev = new Level.JSONLevel(main);
        preferences.putString("Level",a.toJson(jlev, Level.JSONLevel.class));
        preferences.flush();
    }

    public static Level getLevel(){
    Level level;
    Gson m = new Gson();
    Level.JSONLevel jlev;
        Preferences preferences = Gdx.app.getPreferences("MAIN");
        //preferences.putBoolean("Start",false);
        String jsons =preferences.getString("Level");
        if (preferences.getBoolean("Start")){ // инциализация уровня
            jlev =m.fromJson(jsons,Level.JSONLevel.class);
            level= new Level(jlev);
        } else {
            preferences.putBoolean("Start",true);
            m=new Gson();
            level=new Level();
            jlev=new Level.JSONLevel(level);
            preferences.putString("Level",m.toJson(jlev,Level.JSONLevel.class));
        }
        preferences.flush();
        return  level;
    }


}
