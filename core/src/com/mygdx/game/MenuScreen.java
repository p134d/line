package com.mygdx.game;



import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;

import java.util.LinkedList;

import javax.lang.model.type.ArrayType;
import javax.swing.GroupLayout;
import javax.swing.text.Style;

/**
 * Created by Марина on 05.02.2017.
 */

public class MenuScreen  implements Screen {
    public static final int fontsize=128;
    int H=Gdx.graphics.getHeight();
    int W=Gdx.graphics.getWidth();
    float wbtn=W/1.6f;
    float hbtn=wbtn/3.5f;

     static Map stage;
    public static MyGame game;
    Component.ScrollBallPane pane;
    static BitmapFont mainfont;
    static Level level;
    TextButton startbtn;
    TextButton levelbtn;
    TextButton.TextButtonStyle buutonstyle;
    Line lineup;
    Line linedown;
    Component.WindowView window;
    Label myballs;
    Label.LabelStyle mainlstyle;
    Component.GenerateButton generateButton;
    Configuration config;
    Component.SelectBoxLabel selectmode;




    public void setGame(MyGame game) {
        this.game = game;
    }


    public MenuScreen() {
        mainfont = new BitmapFont(Gdx.files.internal("Fonts/one.fnt"));
        mainfont.setColor(Color.WHITE);
        config = new Configuration();
    }

    @Override
    public void show() {
        Configuration.pauseanimation=false;
        level=config.getLevel();
        mainlstyle=new Label.LabelStyle(mainfont,Color.WHITE);
        Table maintable = new Table();
        pane = new Component.ScrollBallPane(maintable,new ScrollPane.ScrollPaneStyle(),level);
        ArrayList<Integer> simpletypes = new ArrayList<Integer>();
        for (Integer i: level.types.keySet()) {
        simpletypes.add(i);
        }
        Collections.sort(simpletypes);
        for (int i=0; i<simpletypes.size();i++) {
            MainBall next = new MainBall(simpletypes.get(i));
            next.show=true;
            next.setSize((Gdx.graphics.getHeight()/5.2f),Gdx.graphics.getHeight()/5.2f);
            maintable.add(next).padRight(Gdx.graphics.getWidth()/7.2f);
            if (level.usestypes.contains(next.getType())) {
                pane.setListenerOnBall(next);
            }
        }
        maintable.row();
        for (int i=0; i<simpletypes.size();i++){
        Label label = new Label("",mainlstyle);
        label.setColor(Color.DARK_GRAY);
        label.setFontScale(Gdx.graphics.getHeight()/21f/fontsize);
            if (level.usestypes.contains(simpletypes.get(i))){
            label.setText(level.strtype.get(simpletypes.get(i)).toString());
            } else {
                label.setText("LEVEL "+level.types.get(simpletypes.get(i)));
            }
            label.setAlignment(Align.center);
            maintable.add(label).width(Gdx.graphics.getHeight()/5f).pad(Gdx.graphics.getWidth()/60,0,Gdx.graphics.getWidth()/35,Gdx.graphics.getWidth()/7.2f);
        }
        maintable.center().pad(0,Gdx.graphics.getHeight()/25,0,Gdx.graphics.getHeight()/25);
        stage = new Map(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);
        stage.addActor(pane);
        makeMenu();





        //Actions
        selectmode.setTouchable(Touchable.disabled);

        startbtn.addAction(Actions.moveTo(startbtn.getX(),startbtn.getY(),1f,Interpolation.smooth2));
        startbtn.addAction(Actions.alpha(0,0));
        startbtn.addAction(Actions.alpha(1,1f));
        startbtn.setY(startbtn.getY()*0.5f);

        selectmode.addAction(Actions.sequence(Actions.delay(0.6f),
                Actions.parallel(Actions.moveTo(selectmode.getX(),selectmode.getY(),0.5f,Interpolation.smooth),Actions.alpha(1,0.5f))));
        selectmode.setY(selectmode.getY()*0.5f);
       selectmode.addAction(Actions.alpha(0,0));

        generateButton.addAction(Actions.sequence(Actions.delay(0.8f),
                Actions.parallel(Actions.moveTo(generateButton.getX(),generateButton.getY(),0.5f,Interpolation.smooth),Actions.alpha(1,0.5f))));
        generateButton.setY(generateButton.getY()*0.4f);
        generateButton.addAction(Actions.alpha(0,0));




        myballs.addAction(Actions.sequence(Actions.delay(1f),
        Actions.parallel(Actions.moveTo(myballs.getX(),myballs.getY(),0.5f,Interpolation.smooth),Actions.alpha(1,0.5f))));
        myballs.setY(myballs.getY()*0.4f);
        myballs.addAction(Actions.alpha(0,0));

            pane.addAction(Actions.alpha(0,0));
        pane.addAction(Actions.sequence(Actions.delay(1.4f),Actions.parallel(Actions.moveTo(pane.getX(),pane.getY(),0.75f,Interpolation.smooth2),Actions.alpha(1,0.75f))));
        pane.setY(pane.getY()*0.1f);

            levelbtn.addAction(Actions.sequence(Actions.delay(1.75f),Actions.parallel(Actions.moveTo(levelbtn.getX(),levelbtn.getY(),0.5f,Interpolation.smooth2),Actions.alpha(1,0.5f)),
            Actions.run(new Runnable() {
                @Override
                public void run() {
                    selectmode.setTouchable(Touchable.enabled);
                    ChangeMODE(true);
                }
            })));
        levelbtn.addAction(Actions.alpha(0,0));
        levelbtn.setY(-levelbtn.getHeight());








    }

    public void makeMenu(){
        generateButton = new Component.GenerateButton(level);


    Skin start = new Skin(new TextureAtlas(Gdx.files.internal("Interface/data_start_btn.txt")));
        buutonstyle=new TextButton.TextButtonStyle(start.getDrawable("up"),start.getDrawable("down"),start.getDrawable("down"),mainfont);
    startbtn= new TextButton("PLAY",buutonstyle);
    startbtn.getLabel().setFontScale(H/11f/fontsize);
    startbtn.setSize(wbtn,hbtn);
    startbtn.setPosition(W/2-startbtn.getWidth()/2,H*0.85f-startbtn.getHeight()/2);
    startbtn.addListener(new ClickListener(){
        @Override
        public void clicked(InputEvent event, float x, float y) {
        if (Configuration.MODE==1){
            game.showGame();} else {
            game.setScreen(new InfinityScreen());
        }
            super.clicked(event, x, y);
        }
    });
    stage.addActor(startbtn);

        Label infinitymode = new Label("INFINITY MODE",mainlstyle);
        Label normalmode = new Label("NORMAL MODE",mainlstyle);
        ArrayList<Label> add= new ArrayList<Label>();
        add.add(infinitymode);
        add.add(normalmode);
        if (Configuration.MODE == 1) {
            selectmode= new Component.SelectBoxLabel(add,normalmode);
        } else {
            selectmode= new Component.SelectBoxLabel(add,infinitymode);
        }
        selectmode.setSize(wbtn,hbtn);
        selectmode.setPosition(W/2-selectmode.getWidth()/2,startbtn.getY()-Line.SIZE_LINE/4-selectmode.getHeight());
      //  ChangeMODE(true);
        stage.addActor(selectmode);
        stage.addActor(generateButton);









    generateButton.setSize(wbtn,hbtn);
    generateButton.setPosition(W/2-startbtn.getWidth()/2,selectmode.getY()-selectmode.getHeight()-Line.SIZE_LINE/4);


    linedown = new Line(1,1,level);
    lineup = new Line(1,1,level);
    int x=-Line.SIZE_LINE;
    int y=(int)(generateButton.getY()-Line.SIZE_LINE*1.25);
    int y1=(int)(startbtn.getY()+startbtn.getHeight()+Line.SIZE_LINE/4);

    for (int i=0; i<W/Line.SIZE_LINE+2;i++){
            linedown.line.add(new Line.SectionLine(x,y,1,0,1));
            lineup.line.add(new Line.SectionLine(x,y1,1,0,1));
            x+=Line.SIZE_LINE;
        }
        stage.addActor(linedown);
        stage.addActor(lineup);

        myballs = new Label("BALLS",mainlstyle);
        myballs.setFontScale(H/18f/fontsize);
        myballs.setTouchable(Touchable.disabled);
        myballs.setEllipsis(false);
        myballs.setHeight(myballs.getMinHeight());
        myballs.setColor(Color.DARK_GRAY);
        // stage.addActor(myballs);


     Skin levels = new Skin(new TextureAtlas(Gdx.files.internal("Interface/data_level_btn.txt")));
     buutonstyle=new TextButton.TextButtonStyle(levels.getDrawable("up"),levels.getDrawable("down"),levels.getDrawable("down"),mainfont);
    levelbtn = new TextButton("LEVEL "+level.NumLev,buutonstyle);
    levelbtn.getLabel().setFontScale(H/13f/fontsize);
    levelbtn.setSize(wbtn,hbtn);
    levelbtn.setPosition(W/2-levelbtn.getWidth()/2,levelbtn.getHeight()/3);
    stage.addActor(levelbtn);

        pane.setY(levelbtn.getY()+levelbtn.getHeight()+Line.SIZE_LINE/4);
        pane.setHeight(pane.getPrefHeight());





    }

    public void ChangeMODE(boolean start){
    if (start && Configuration.MODE==2 && generateButton.getTouchable().equals(Touchable.enabled)){
        generateButton.addAction(Actions.parallel(Actions.sizeTo(1,1,0.5f,Interpolation.smooth),Actions.moveBy(wbtn/2,hbtn/2,0.5f,Interpolation.smooth)));
        generateButton.setTouchable(Touchable.disabled);
        Configuration.MODE=2;
    } else  if (!start){
        if (selectmode.activlabel.getText().toString().equals("NORMAL MODE"))  {
            Configuration.MODE=1;
            generateButton.setTouchable(Touchable.enabled);
            generateButton.addAction(Actions.parallel(Actions.sizeTo(wbtn,hbtn,0.5f,Interpolation.smooth),Actions.moveBy(-wbtn/2,-hbtn/2,0.5f,Interpolation.smooth)));
        } else {
            generateButton.addAction(Actions.parallel(Actions.sizeTo(1,1,0.5f,Interpolation.smooth),Actions.moveBy(wbtn/2,hbtn/2,0.5f,Interpolation.smooth)));
            generateButton.setTouchable(Touchable.disabled);
            Configuration.MODE=2;
        }} else{

    }
    }

    public  void showWindowTypeBall(int type){


        int H = Gdx.graphics.getHeight();
        int W = Gdx.graphics.getWidth();
        int widthw=W;
        int heihgthw=(int)(H/1.7f); // размеры
        Skin a = new Skin();   // подгрузка
        a.add("exit",new Texture(Gdx.files.internal("Interface/exit_btn.png")));
        Skin okbtn = new Skin(new TextureAtlas("Interface/data_ok_btn.txt"));
        TextButton okey  = new TextButton("OK",new TextButton.TextButtonStyle(okbtn.getDrawable("up"),okbtn.getDrawable("down"),okbtn.getDrawable("down"),mainfont));
        okey.setSize(widthw/3.5f,widthw/3.5f);
        okey.getLabel().setFontScale(widthw/5.5f/fontsize);
        Image exit  = new Image(a.getDrawable("exit"));


        Label desription = new Label(level.loadDescriptionBall(type),new Label.LabelStyle(mainfont,Color.BLACK));
        desription.setWrap(true);
        desription.setFontScale(((float)(heihgthw)/12)/fontsize);
        desription.setColor(Color.DARK_GRAY);

        Label title = new Label(Level.strtype.get(type).toString(), new Label.LabelStyle(mainfont,Color.WHITE));
        title.setFontScale(((float)(heihgthw)/6)/fontsize);
        title.setColor(Color.DARK_GRAY);
        title.setWrap(true);
        title.setAlignment(Align.center);



        a.add("back",new Texture(Gdx.files.internal("Interface/backgroud_win.png")));

        window = new Component.WindowView("",new Window.WindowStyle(mainfont,Color.WHITE,a.getDrawable("back")));
        window.setSize(widthw,heihgthw);
        window.add(exit).top().right().size(heihgthw/15,heihgthw/15).expandX().row();

        exit.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                window.close();
                stage.setFocusTouch();
                super.clicked(event, x, y);
            }
        });

        okey.addListener(new ClickListener(){

            @Override
            public void clicked(InputEvent event, float x, float y) {
                window.close();
                stage.setFocusTouch();
                super.clicked(event, x, y);
            }
        });

        MainBall addingball = new MainBall(type);
        addingball.show=true;
        addingball.setSize(heihgthw/3,heihgthw/3);
        Table add = new Table();
        add.pad(widthw/8);


        add.add(title).center().pad(0,heihgthw/60,heihgthw/30,heihgthw/60).fillX();
        add.row();

        add.add(addingball).center();
        add.row();

        add.add(desription).padTop(heihgthw/25).fill().center().minWidth((int)(widthw/1.2));
        add.setPosition(0,H/2-window.getHeight()/2);
        add.row();

        add.add(okey).center().padTop(heihgthw/25).size(widthw/3.5f,widthw/3.5f).getActor().getLabelCell().center();
        add.center().columnDefaults(0).pad(0,widthw/30,widthw/30,0);
        ScrollPane scrolladd  = new ScrollPane(add,new ScrollPane.ScrollPaneStyle());
        scrolladd.setFadeScrollBars(false);
        scrolladd.setScrollingDisabled(true,false);
        window.add(scrolladd).expandX().fillX().center();
        window.pad(widthw/30);
        window.getTitleLabel().setVisible(false);
        window.setMovable(false);
        window.setY((H-heihgthw)/2);
        window.show();
        stage.addActor(window);
        stage.setFocusTouchOnWindow();

    }



    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(1,1,1,1);
        stage.act(delta);
        stage.draw();
        if (window!=null) {
        if (window.destroy) {
        window=null;
        stage.RemoveAllWindow();
        }
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
    Configuration.SaveLevel(level);

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
    Configuration.SaveLevel(level);


    }

    @Override
    public void dispose() {
        Configuration.SaveLevel(level);

    }









}
